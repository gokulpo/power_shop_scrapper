Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'file_processing#new'
  resources :home do
    collection do
      get 'convert'
      get 'list'
      get 'image'
    end
  end
  resources :file_processing, only:[ :new, :create]
end

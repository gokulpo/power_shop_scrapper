class AddColumnsToImage < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :name, :string
    add_column :images, :icp, :string
    add_column :images, :start_date, :string
    add_column :images, :end_date, :string
    add_column :images, :total, :string

  end
end

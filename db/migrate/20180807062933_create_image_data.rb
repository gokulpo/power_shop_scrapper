class CreateImageData < ActiveRecord::Migration[5.1]
  def change
    create_table :image_data do |t|
      t.string :image

      t.timestamps
    end
  end
end

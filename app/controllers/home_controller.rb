class HomeController < ApplicationController
 # require 'pdfcrowd'
 require 'rtesseract'
  def index
    @image = Image.new
  end

  def create
    params[:image].each do |img|
      @image = Image.new(image: img)
      @image.save
      convert(@image)
    end
    @images = Image.all
      respond_to do |format|
        format.js
    end
  end

  def convert(file)
    list = ["contact", "energyonline", "flick", "genesis", "meridian","trust"]
    type = file.image.content_type
    text = []
    if type == "application/pdf"
      reader = PDF::Reader.new(file.image.path)
      reader.pages.each do |page|
        text << page.text
      end
      list.each do |l|
        if text[0].include? l
          send(l+"_processing", text)
        end
      end
    else
      # process_image
      # image = RTesseract.new(Image.last.image.path, :lang => "eng")
      # text = image.to_s
      # store_text

    end
    @images = Image.all
  end

  def list
    @images = Image.all
  end

  def image
  end

  private

  def image_params
     params.require(:image).permit(:image)
  end

  def pdf_pages
    reader = PDF::Reader.new(Image.last.image.path)
    pages_arr = []
    reader.pages.each{|page| pages_arr = page.text.split("\n")}
    pages_arr
  end

  def get_start_date (the_str)
    start_date = the_str[0].split("from ")[1].delete(" to")
  end

  def get_end_date (pages_arr)
    the_line = find_text(pages_arr,"from")
    the_index = pages_arr.index(the_line)
    end_date = pages_arr[1]
  end

  def find_text(the_arr, the_text)
    the_arr.select{|item| item.downcase.include? the_text }
  end

  def contact_processing(text)
    pages_arr = pdf_pages
    name = "Contact Energy Residential Electricity"
    icp = find_text(pages_arr,"icp")[0].split(" ")[14]
    total_cost_line = find_text(pages_arr,"if paid after")[0].split(" ").last
    start_date = get_start_date(find_text(pages_arr,"from"))
    end_date = get_end_date(pages_arr)
    Image.last.update(name: name, icp: icp, total: total_cost_line, start_date: start_date, end_date: end_date)
    puts "**************\n"
    puts "end_date: #{end_date}"
    puts "**************\n"
  end

  def energyonline_processing(text)
    pages_arr = pdf_pages
    name = "Energy Online"
    icp = find_text(pages_arr," icp")[0].split(" ")[2]
    total_cost_line = pages_arr.last.split( " GST ").last.delete("(").delete(")")
    start_date = get_start_date(find_text(pages_arr," from"))[0..7]
    end_date = get_start_date(find_text(pages_arr," from"))[8..15]
    Image.last.update(name: name, icp: icp, total: total_cost_line, start_date: start_date, end_date: end_date)
  end

  def flick_processing(text)

  end

  def genesis_processing(text)

  end

  def meridian_processing(text)

  end

  def trust_processing(text)

  end
end

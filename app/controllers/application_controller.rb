class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :allow_facebook_iframe
  
  private

  def allow_facebook_iframe
    response.headers['X-Frame-Options'] = 'ALLOWALL'
  end
end

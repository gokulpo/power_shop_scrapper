class Image < ApplicationRecord
  require 'rmagick'
  mount_uploader :image, ImageUploader
end
